#!/usr/bin/python3


"""

On the instance:
-----------------

echo "curl https://gitlab.com/NiekWijs/bigdatazookeeper/-/raw/main/app.py > app.py && sudo pip3 install flask && screen -m -d  sudo python3 app.py" | ssh -i ../BigDataOpdrKeyPair.pem ec2-user@ec2-34-238-80-205.compute-1.amazonaws.com



curl https://gitlab.com/NiekWijs/bigdatazookeeper/-/raw/main/app.py > app.py &&
    sudo pip3 install flask &&
    screen -m -d  sudo python3 app.py


"""

from flask import Flask, render_template_string
from sys import argv
import socket
from kazoo.client import KazooClient


ZK_URL = "ec2-54-210-12-84.compute-1.amazonaws.com"

app = Flask(__name__)


class PRODUCT:
    def __init__(self, product_name, price_in_dollars):
        self.product_name = product_name
        self.price_in_dollars = price_in_dollars


class DATABASE:
    def __init__(self, connection_string):
        self.connection_string = connection_string

    def get_all_products(self):
        database_data = None
        if self.connection_string == \
                "Northern-American-Database-Connectionstring":
            database_data = [
                PRODUCT("Maple sirup", 20),
                PRODUCT("Freedom", 1),
                PRODUCT("Murican gun", 80),
                PRODUCT("Pickup truck", 67),
                PRODUCT("Hamburgers", 500),
                PRODUCT(
                    "A number 9 large a number 6 with extra dip, and a large soda.", 1234)
            ]

        elif self.connection_string == \
                "Asian-Datrabase-Connectionstring":
            database_data = [
                PRODUCT("Spicy noodels", 1000),
                PRODUCT("Chopsticks", 1),
                PRODUCT("Chinese-wall fence", 4),
                PRODUCT("Tokyo tower", 1),
                PRODUCT("Jun kook", 4523),
                PRODUCT("North korean mini-nuke set", -1)
            ]

        elif self.connection_string == \
                "European-Database-Connectionstring":
            database_data = [
                PRODUCT("Stroopwafeltjes", 2),
                PRODUCT("Lederhosen", 70),
                PRODUCT("French cheese", 50),
                PRODUCT("Brussels sproutes", 2),
                PRODUCT("Pizza", 11),
                PRODUCT("Ikea diy table", 3),
            ]

        return database_data


class ZOOKEEPER_INTERFACE:

    def __init__(self, aws_region):
        self.aws_region = aws_region
        self.zookeeper_node_region = self.determine_zookeeper_region_node_from_aws_region(
            self.aws_region)
        self.zk_client = KazooClient(hosts=ZK_URL)
        self.zk_client.start()
        self.znode_data = self.get_znode_data()
        self.create_ephemeral_self_znode()

    """

    Determine what z-node to get the data from.
    In real life this information would come from the E2 region,
    but because we're on a student plan we only have access to one
    region. In this case we'll simulate it by manyally inputting it as an arg.

    In this case we'll simulate the following regions:

    * us-east-1
    * ap-south-1
    * eu-west-2

    """

    def determine_zookeeper_region_node_from_aws_region(self, aws_region):
        global_region = aws_region.split("-")[0]
        if global_region in ["us", "ca", "sa"]:
            return "NAmerica"
        elif global_region in ["eu", "me", "af"]:
            return "Europe"
        elif global_region in ["ap"]:
            return "Asia"

    def get_own_ipv4_address(self):
        hostname = socket.gethostname()
        hostbyname = socket.gethostbyname(hostname)
        return hostbyname

    def create_ephemeral_self_znode(self):
        own_ipv4 = self.get_own_ipv4_address().replace(".", "_")
        znode_path = "/webapps/" + self.zookeeper_node_region + "/" + own_ipv4
        try:
            self.zk_client.create(znode_path, ephemeral=True)
        except:
            pass

    def get_znode_data(self):
        znode_dict = {}
        znode_path = "/webapps/" + self.zookeeper_node_region
        znode_data = self.zk_client.get(znode_path)[0].decode("utf-8")
        znode_data_lines = znode_data.split("\n")
        for line in znode_data_lines:
            if line:
                key, value = line.split(": ")
                znode_dict[key] = value
        return znode_dict

    def zookeeper_get_instance_continent(self):
        return self.znode_data["Continent"]

    def zookeeper_get_instance_database_connection_string(self):
        return self.znode_data["Connection_str"]

    def zookper_get_instance_local_time(self):
        return self.znode_data["Local_time"]

    def zookper_get_instance_currency_used(self):
        return self.znode_data["Currency_used"]

    def zookeeper_get_instance_currency_conversion_rate(self):
        return int(self.znode_data["Conversion_rate"])


class SHOP_ITEMS_REPOSITORY:

    """
    This emulates a repository pattern.
    In a real word application a database would be used.
    """

    def __init__(self):

        aws_region = argv[1]
        self.zookeeper_interface = ZOOKEEPER_INTERFACE(aws_region)

        # Get instance metadata from zookeeper
        self.instance_local_time = self.zookeeper_interface.zookper_get_instance_local_time()
        self.instance_continent = self.zookeeper_interface.zookeeper_get_instance_continent()
        self.currency_used = self.zookeeper_interface.zookper_get_instance_currency_used()
        self.conversion_rate = self.zookeeper_interface.zookeeper_get_instance_currency_conversion_rate()

        # Get data from the "database"
        self.instance_database_connection_string = self.zookeeper_interface.zookeeper_get_instance_database_connection_string()
        self.database = DATABASE(self.instance_database_connection_string)

    def get_products(self):
        products = []
        for product in self.database.get_all_products():
            product_usd_price = product.price_in_dollars
            product_conversion_rate = self.conversion_rate
            product.converted_price = product_usd_price * product_conversion_rate
            products.append(product)
        return products


@app.route("/")
def homepage():

    html = """

    <!DOCTYPE html>
    <html>
    <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
        * {
        box-sizing: border-box;
        }
    
        .column {
            float: left;
            width: 25%;
            padding: 0 10px 20px;
            margin-left: 40px;
            margin-right: 40px;
            margin-top: 50px;
        }

        .row {margin: 0 -5px;}

        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        @media screen and (max-width: 600px) {
        .column {
            width: 100%;
            display: block;
            margin-bottom: 20px;
            }
        }

        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            padding: 16px;
            text-align: center;
            background-color: #f1f1f1;
        }

        # container{width:100%;}
        # left{float:left;width:100px;}
        # right{float:right;width:100px;}
        # center{margin:0 auto;width:100px;}

        </style>
    </head>
    <body>

    <h2>Multi-National-Corpo BV. Webshop™©®</h2>
    <div>
        <p>{{ instance_continent }}</p>
        <p>Local time is: {{local_time}}</p>
    </div>

    <div class="row">
    {% for product in  products %}
        <div class="column">
            <div class="card">
                <h4><b>{{ product.product_name }}</b></h4> 
                <p>{{ product.converted_price }} {{currency_used}}</p> 
                <button onclick="alert('Out of stock!')">Order</button>
            </div>
        </div>
    {% endfor %}
    </div>
    </body>
    </html>
    """
    return render_template_string(
        html,
        local_time=shop_repo.instance_local_time,
        instance_continent=shop_repo.instance_continent,
        products=shop_repo.get_products(),
        currency_used=shop_repo.currency_used
    )


if __name__ == '__main__':
    shop_repo = SHOP_ITEMS_REPOSITORY()
    app.run(debug=True, host="0.0.0.0", port=80)
